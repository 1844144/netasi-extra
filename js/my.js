
function do_js_count_table(){
	// row by row
	// if 2 or more is text (or one in case of 2 column table)
	// 	it is start of table 
	//  	if total is in row - accumulate total
	//		in any case accumulate values from columns

	//  if start with total > it is end of calc area 
	//  if start with 2 or more text values = it is new table area go to 1

	var tables = document.getElementsByClassName('js-count-total-2');
	var i,j,z;

	function TheTable(table_to_init){
		var table = table_to_init;
		this.get = function(i,j) {
			var text = jQuery(table.rows[i].cells[j]).text();
			return text.replace(/[\n\ ]*/gm,"");
		}
		this.set = function(i,j,val){
			return jQuery(table.rows[i].cells[j]).text(val);
		}
	}

	var count_of_text,val;
	for (z=0; z<tables.length; z++ ){
		t = new TheTable(tables[z]);
		var filling = false;
		var has_total_column =false;
		var height = tables[z].rows.length;
		var sums;

		for (i=0; i<height; i++){
			count_of_text = 0;
			var width = tables[z].rows[0].cells.length;
			// check for text 
			var found_total = false;
			var begins_with_total = false;
			for (j=0; j<width; j++){
					val = t.get(i,j);
					if (val!='' && isNaN(parseInt(val)) ) count_of_text++;
					if (j==width-1) found_total = ( val=='Total');
					if (j==0) begins_with_total = (val=='Total');
			}

			if (count_of_text >= 2) {
				// so this is new header
				sums = [];
				has_total_column = found_total;
				filling = true;
			}
			else {
				if (filling) {
					if(begins_with_total){
						filling = false;
						// and print all sums
						for (j=1;j<width; j++) 
							if (j in sums) t.set(i,j, sums[j]);
					}
					else {
						// this is a ususal fow to fill
						var row_sum  = 0;
						var limit = has_total_column? width-1: width;
						for (j=1;j<limit; j++) {
							val = parseInt( t.get(i,j) );
							if (! isNaN(val) ){
								row_sum += val;
								if (j in sums) sums[j] += val;
								else sums[j] = val;
							}
						}
						if (has_total_column){
							t.set(i,limit,row_sum);
							if (limit in sums) sums[limit] += row_sum;
							else sums[limit] = row_sum;
						}
					}
				}
			}	
		}
	}
}


jQuery(document).ready(function(){
	// check if we need to use table export 
	do_js_count_table();
	if (jQuery('table.js-count-total').length != 0){
		jQuery('table.js-count-total > tbody > tr').each(function(ind,obj){
			td1 = jQuery(obj).find('td:first-child');
			td2 = jQuery(obj).find('td:nth-child(2)');
			html = td1.html();
			text = td1.text();

			if (html){
				if (html.includes("<h3>")){
					if (text.includes('All Meals')){
						window.we_on_all_meals = true;
					}
					else if (text.includes('Meals')) {
						window.this_is_meals = true;
					}
					else {
						window.this_is_meals = false;
					}
					window.my_count = 0;
					window.my_start_count = true;
					console.log('start');
				}
				else if (td1.text()=='Total'){
					window.my_start_count = false;
					if (window.we_on_all_meals){
						td2.html('<strong>'+window.meals_total+'</strong>');
						window.we_on_all_meals = false;
					}else{
						if (window.this_is_meals){
							if (! window.meals_total) window.meals_total = 0;
							window.meals_total += window.my_count;
							window.this_is_meals = false;
						}
						td2.html('<strong>'+window.my_count+'</strong>');
					}
					console.log(window.my_count);
					window.my_count = 0; 
				}
				else {
					if (window.my_start_count == true){
						console.log(td2.text());
						window.my_count += parseInt(td2.text());
					}
				}

			}
		});

	}
});